const axios = require('axios')
const fs = require('fs')

module.exports = async () => {
    const res = await axios.get('https://minfin.com.ua/currency/nbu/')
    const { data } = res

    const currency = {}
    
    try {
        currency.usd = data.match(/<tr class="row--collapse">\n<td(.*>)\n<div[A-Za-z=\s"-]*>\s\d+\.\d+\n<span[A-Za-z=\s"-]*>\d+<\/span> грн\n<span[A-Za-z=\s"-]*>(\+|-) \d.\d*<\/span>\n<\/div>\n<a href="\/currency\/nbu\/usd\/">ДОЛЛАР<\/a>\n\n?<\/td>\n<td class="[A-Za-z\s-]*" data-title="Курс НБУ">\n(\d+.\d+)/m)[3]
        currency.eur = data.match(/<tr class="row--collapse">\n<td(.*>)\n<div[A-Za-z=\s"-]*>\s\d+\.\d+\n<span[A-Za-z=\s"-]*>\d+<\/span> грн\n<span[A-Za-z=\s"-]*>(\+|-) \d.\d*<\/span>\n<\/div>\n<a href="\/currency\/nbu\/eur\/">ЕВРО<\/a>\n\n?<\/td>\n<td class="[A-Za-z\s-]*" data-title="Курс НБУ">\n(\d+.\d+)/m)[3]
        currency.rub = data.match(/<tr class="row--collapse">\n<td(.*>)\n<div[A-Za-z=\s"-]*>\s\d+\.\d+\n<span[A-Za-z=\s"-]*>\d+<\/span> грн\n<span[A-Za-z=\s"-]*>(\+|-) \d.\d*<\/span>\n<\/div>\n<a href="\/currency\/nbu\/rub\/">РУБЛЬ<\/a>\n\n?<\/td>\n<td class="[A-Za-z\s-]*" data-title="Курс НБУ">\n(\d+.\d+)/m)[3]

        return currency
    } catch (e) {
        return { error: 'Something went wrong. Check your RegExps!' }
    } 
}