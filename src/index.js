require('dotenv').config()
require('./db/mongoose')

const Currency = require('./models/currency')
const Log = require('./models/log')
const parseCurrency = require('./utils/parseCurrency')

const interval = setInterval(async () => {
    const currency = await parseCurrency()

    if (currency.error) {
        await new Log({ error: currency.error }).save()

        clearInterval(interval)
    }

    await Currency.insertMany([
        {
            currency: 'usd',
            exchangeRate: currency.usd
        },
        {
            currency: 'eur',
            exchangeRate: currency.eur
        },
        {
            currency: 'rub',
            exchangeRate: currency.rub
        }
    ])
}, 60000)