const mongoose = require('mongoose')

const CurrencySchema = new mongoose.Schema({
    currency: String,
    exchangeRate: Number
}, {
    timestamps: true
})

module.exports = mongoose.model('currency', CurrencySchema)