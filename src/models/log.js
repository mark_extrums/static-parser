const mongoose = require('mongoose')

const LogSchema = new mongoose.Schema({
    error: String
}, {
    timestamps: true
})

module.exports = mongoose.model('log', LogSchema)